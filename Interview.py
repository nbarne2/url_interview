import numpy as np
import operator
import json

def url_calc():
    # Input sample array from memory with hit rates and URL
    array_1 = [600, "yahoo.com", 30, "google.uk", 120,
               "starwars.com", 50, "foot.ball.org", 200,
               "corn.com", 10, "battle.org", 20, "pop.corn.com"]
    # Convert array to dictionary
    my_dict = dict(zip(i := iter(array_1), i))
    # Iterate over values, substring with . delimiter, add unique instances into array
    # key will be the sum total of all matches in all values
    # Declare array for appending substrings
    substr_array = []
    # String split command builds 2D array, convert back to 1d
    array1d = []
    for value in my_dict.values():
        if "." in value:
            substr = value.split(".", -1)
            substr_array.append(substr)
    one_array = np.array(substr_array, dtype=object)
    for x in one_array:
        for y in x:
            array1d.append(y)
    # Remove duplicates
    clean_array = []
    [clean_array.append(x) for x in array1d if x not in clean_array]
    # For each substring, determine how many times it exists in the dictionary
    # Add values of each Key (URL hit rate) and aggregate values then insert into dictionary
    for i in clean_array:
        counter = 0
        for value in my_dict.values():
            if i in value:
                counter += 1
        # Ignore cases where URL and substring are equal
        # This data will be duplicate like yahoo = yahoo.com so not necessary to include data
        if counter > 1:
            url_count = 0
            for key, value in my_dict.items():
                if i in value:
                    url_count += key
            # Add new dictionary entry with summed values
            my_dict[url_count] = i
    # Add header to beginning of dictionary and print in JSON
    header_dict = {"URL_Count": "Website"}
    # Sort by keys descending
    sorted_dict = sorted(my_dict.items(), key=operator.itemgetter(0), reverse=True)
    header_dict.update(sorted_dict)
    json_object = json.dumps(header_dict, indent=4)
    print(json_object)

def main():
    url_calc()

if __name__ == "__main__":
    main()