# Takes an input of an array with website URLs and hit counter and returns a JSON object list of URLs and substrings of the URLs (except for the ones which would equal the same as the URL for example yahoo.com and yahoo)

**Input**:
`[600, "yahoo.com", 30, "google.uk", 120,
               "starwars.com", 50, "foot.ball.org", 200,
               "corn.com", 10, "battle.org", 20, "pop.corn.com"]`

**Output**:
`{
    "URL_Count": "Website",
    "940": "com",
    "600": "yahoo.com",
    "220": "corn",
    "200": "corn.com",
    "120": "starwars.com",
    "60": "org",
    "50": "foot.ball.org",
    "30": "google.uk",
    "20": "pop.corn.com",
    "10": "battle.org"
}`
